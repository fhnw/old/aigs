package ch.fhnw.richards.aigs;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import ch.fhnw.richards.aigs.gameLogic.Game;
import ch.fhnw.richards.aigs.gameLogic.GameTypes;

/**
 * This class provides the listener-socket for AIGS, and is responsible for managing players and
 * (through the players) the list of active games.
 * 
 * When players want to register new games or join existing ones, they call methods
 * provided in this class, in order to carry out these actions.
 * 
 * @author brad
 * 
 */
public class GameManager extends Thread {
	private int portNumber;

	private ServerSocket listener;

	// Variable to control thread execution
	private boolean runGameManager;

	public GameManager(int portNumber) {
		this.portNumber = portNumber;
		
		this.setName("AIGS game manager");
		runGameManager = true;
	}
	
	public void stopGameManager() {
		runGameManager = false;
		try {
			listener.close();
		} catch (IOException e) {
			// Closing the socket terminates the "accept" method,
			// and causes an exception, which we ignore
		}
	}

	@Override
	public void run() {
		try {
			listener = new ServerSocket(portNumber);
			while (runGameManager) {
				Socket playerSocket = listener.accept();
				
				// Pass the socket on to a player-object
				Player newPlayer = new Player(this, playerSocket);
				
				// The player object processes its own join/create commands
				newPlayer.joinGame();
			}
		} catch (IOException e) {
			// Throws a SocketException when stopGameManager closes the socket
		} finally {
			try {
				listener.close();
			} catch (IOException e) {
			}
		}
	}
}
