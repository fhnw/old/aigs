package ch.fhnw.richards.aigs;

import java.util.ArrayList;

/**
 * A game position is <i>conceptually</i> a multidimensional array of integers. Since the number
 * and sizes of the dimensions are not know at compile time, we instead represent this as a single-dimensional
 * array, and provide custom <code>get</code> and <code>set</code> methods to access individual elements.
 * 
 * This class also allows access to the raw one-dimensional array, in case the caller wants to do its
 * own processing.
 * 
 * @author brad
 * 
 */
public class GamePosition {
	private char[] gamePosition;
	private int[] dimensions;
	private int size;

	/**
	 * Create a new game-position of the specified dimensions. For example, to create a TicTacToe
	 * board, use <code>new GamePosition(3, 3)</code>
	 * 
	 * New GamePositions are initialized to contain -1 in all positions.
	 * 
	 * @param dimensions
	 *          The integer dimensions for the game position.
	 */
	public GamePosition(int... dimensions) {
		this.dimensions = dimensions;
		
		int size = 1;
		for (int dim : dimensions) {
			size *= dim;
		}
		this.size = size;
		gamePosition = new char[size];
		
		for (int i = 0; i < size; i++) gamePosition[i] = ' ';
	}

	/**
	 * Get a particular value in the gamePosition
	 * 
	 * @param indices The indices into the multidimensional gamePosition
	 * @return
	 */
	public char get(int... indices) {
		return gamePosition[getRealPosition(indices)];
	}

	/**
	 * Set a particular value in the game Position
	 * 
	 * @param value The new value
	 * @param indices The indices into the multidimensional gamePosition
	 */
	public void set(char value, int... indices) {
		gamePosition[getRealPosition(indices)] = value;		
	}

	/**
	 * Get the raw (one-dimensional) gamePosition. When doing extensive calculations, it may be more
	 * efficient for the client to convert this into the appropriate multidimensional representation.
	 * 
	 * @return The raw gamePosition
	 */
	public char[] getRawPosition() {
		return gamePosition;
	}
	
	/**
	 * Set the raw (one-dimensional) gamePosition. When doing extensive calculations, it may be more
	 * efficient for the client to convert this into the appropriate multidimensional representation.
	 * 
	 * @param newPosition The new raw gamePosition
	 */
	public void setRawPosition(char[] newPosition) {
		if (newPosition.length != size) throw new ArrayIndexOutOfBoundsException(); // Array is the wrong size
		gamePosition = newPosition;
	}
	
	private int getRealPosition(int... indices) {
		int realPosition = indices[0];		
		for (int i=1; i < indices.length; i++) {
			realPosition = realPosition * dimensions[i] + indices[i];
		}
		return realPosition;
	}
}
