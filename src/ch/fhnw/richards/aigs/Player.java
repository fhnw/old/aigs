package ch.fhnw.richards.aigs;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import ch.fhnw.richards.aigs.gameLogic.Game;
import ch.fhnw.richards.aigs.gameLogic.GameStatus;
import ch.fhnw.richards.aigs.gameLogic.GameTypes;
import ch.fhnw.richards.aigs.messages.*;

/**
 * This class represents a single player, communicating via a socket,
 * participating in a particular game. The socket is read in a thread,
 * which is named as soon as we have the necessary information from the
 * player
 * 
 * @author brad
 *
 */
public class Player extends AbstractPlayer {
	// List of all current players
	private static HashSet<Player> players = new HashSet<Player>();
	
	private Thread thread;
	private Game game;
	private String playerName;
	
	public static void addPlayer(Player newPlayer) {
		synchronized(players) {
			players.add(newPlayer);
		}
	}
	
	public static void removePlayer(Player oldPlayer) {
		synchronized(players) {
			players.remove(oldPlayer);
		}
	}
	
	public static int getNumPlayers() {
		return players.size();
	}
	
	public Player(GameManager manager, Socket playerSocket) {
		super(playerSocket);
		this.thread = new Thread();
	}
	
	/**
	 * Incoming player command. When a player contacts the server, the only acceptable command is the join-game
	 * command. This method receives, checks and processes the join-game command.
	 */
	public void joinGame() {
		MessageJoinGame msg = (MessageJoinGame) receive(AigsMessageTypes.MESSAGE_JOINGAME);
		
		GameTypes gameType = msg.getGameType();
		String gameID = msg.getGameID();
		int maxPlayers = msg.getNumPlayers();
		playerName = msg.getPlayerName();
		
		// Get the game if it exists; otherwise create it
		Game g;
		try {
			g = Game.joinOrCreateGame(gameType, gameID, maxPlayers, this);
			Message reply = new MessageResult(true, "game joined");
			Player.addPlayer(this);
			game = g;
			send(reply);
		} catch (AigsException e) {
			Message reply = new MessageResult(false, e.getMessage());
			send(reply);
			close();
		}
	}
	
	/**
	 * Outgoing game status. Give the player the current game position.
	 */
	public void sendPosition() {
		MessageGameStatus msg = game.makeGameStatusMessage();
		send(msg);		
	}
	
	/**
	 * Incoming player move. The player was given a game position; now
	 * we read the player's desired move.
	 */
	public MessageGameMove getMove() {		 
		MessageGameMove msg = (MessageGameMove) receive(AigsMessageTypes.MESSAGE_GAMEMOVE);
		return msg;
	}
	
	/**
	 * Outgoing game status. The game is over. Send the final position and result
	 * to this player, then terminate the player connection.
	 */
	public void gameOver() {
		MessageGameStatus msg = game.makeGameStatusMessage();
		send(msg);
		this.close();
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	@Override
	public boolean equals(Object o) {
		Player p = (Player) o;
		return playerName.equals(p.playerName);
	}
}
