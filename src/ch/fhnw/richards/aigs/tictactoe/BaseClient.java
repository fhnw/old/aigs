package ch.fhnw.richards.aigs.tictactoe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ch.fhnw.richards.aigs.GamePosition;
import ch.fhnw.richards.aigs.gameLogic.GameStatus;
import ch.fhnw.richards.aigs.gameLogic.GameTypes;
import ch.fhnw.richards.aigs.messages.AigsMessageTypes;
import ch.fhnw.richards.aigs.messages.Message;
import ch.fhnw.richards.aigs.messages.MessageGameMove;
import ch.fhnw.richards.aigs.messages.MessageGameStatus;
import ch.fhnw.richards.aigs.messages.MessageJoinGame;
import ch.fhnw.richards.aigs.messages.MessageResult;

/**
 * The simplest way to create a new TicTacToe client is to create a subclass of
 * this class. Then override the method <code>makeMove(GamePosition)</code> and
 * implement your game logic.
 * 
 * @author brad
 * 
 */
public abstract class BaseClient extends JFrame {
	/**
	 * This method contains the game-logic that determines where to play.
	 * We calculate our next move and return it as indices into the board. For
	 * example, to move to the center position, return [1,1]
	 * 
	 * @return the move that we want to make
	 */
	protected abstract int[] makeMove(GamePosition gamePosition);	
	
	/**
	 * This method identifies which player we are
	 * 
	 * @return our player name (X or O)
	 */
	protected char getPlayerName() {
		return playerName;
	}
	
	// ---------------------------------------------------------------
	// ---------------- Abstract class implementation ----------------
	// ---------------------------------------------------------------	
	
	// Divide by 10 to get the displayed version number
	private static final long serialVersionUID = 1;

	// Instance number, in case multiple clients are running in the same JVM
	// Note: normally unnecessary, since each program runs in its own JVM.
	private static int instanceCounter = 0;
	private int instanceNumber;

	// Various important instance variables
	private String clientName; // Name displayed in title bar
	private Logger logger; // Logger prints debug-info to console
	private Socket aigsServer; // Socket connected to AIGS-server
	private char playerName; // Which player are we? 'X' or 'O'

	// Important GUI controls
	private JTextField txtIpAddress;
	private JTextField txtPortNumber;
	private JRadioButton btnPlayerX;
	private JRadioButton btnPlayerO;
	private JTextField txtGameName;
	private Container pnlGameBoard;
	private JButton btnStart;

	// The game board, as JLabels
	private JLabel[][] gameBoard;
	private GamePosition gamePosition = new GamePosition(3, 3);

	protected BaseClient() {
		// Create upper GUI elements
		Box topBox = Box.createHorizontalBox();
		Box leftControls = Box.createVerticalBox();
		Box leftTopControls = Box.createHorizontalBox();
		Box leftBottomControls = Box.createHorizontalBox();
		leftControls.add(leftTopControls);
		leftControls.add(leftBottomControls);

		JLabel lblIpAddress = new JLabel("IP address");
		txtIpAddress = new JTextField("127.0.0.1");
		JLabel lblPort = new JLabel("Port");
		txtPortNumber = new JTextField("50001");
		leftTopControls.add(lblIpAddress);
		leftTopControls.add(Box.createHorizontalStrut(5));
		leftTopControls.add(txtIpAddress);
		leftTopControls.add(Box.createHorizontalStrut(15));
		leftTopControls.add(lblPort);
		leftTopControls.add(Box.createHorizontalStrut(5));
		leftTopControls.add(txtPortNumber);

		JLabel lblGameName = new JLabel("Game name");
		txtGameName = new JTextField("TicTacToe 1");
		JLabel lblPlayerNumber = new JLabel("Player");
		leftBottomControls.add(lblGameName);
		leftBottomControls.add(Box.createHorizontalStrut(5));
		leftBottomControls.add(txtGameName);
		leftBottomControls.add(Box.createHorizontalStrut(15));
		leftBottomControls.add(lblPlayerNumber);
		btnPlayerX = new JRadioButton(Players.X.toString());
		btnPlayerX.setSelected(true);
		leftBottomControls.add(btnPlayerX);
		btnPlayerO = new JRadioButton(Players.O.toString());
		leftBottomControls.add(btnPlayerO);
		ButtonGroup grpPlayer = new ButtonGroup();
		grpPlayer.add(btnPlayerX);
		grpPlayer.add(btnPlayerO);

		btnStart = new JButton("Connect");

		topBox.add(leftControls);
		topBox.add(Box.createHorizontalStrut(15));
		topBox.add(btnStart);
		topBox.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Create game board (3x3)
		pnlGameBoard = new JPanel();
		pnlGameBoard.setPreferredSize(new Dimension(300, 300));
		pnlGameBoard.setLayout(new GridLayout(3, 3));
		gameBoard = new JLabel[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				JLabel tmpLabel = new JLabel("");
				tmpLabel.setHorizontalAlignment(SwingConstants.CENTER);
				tmpLabel.setBorder(new LineBorder(Color.BLACK, 1));
				tmpLabel.setName(i + "," + j);
				gameBoard[i][j] = tmpLabel;
				pnlGameBoard.add(tmpLabel);
			}
		}

		// Set up window
		instanceNumber = ++instanceCounter;
		clientName = "AIGS TicTacToe Client - v" + serialVersionUID / 10 + "."
				+ serialVersionUID % 10 + " - " + instanceNumber;
		this.setTitle(clientName);

		// Set up logger
		logger = Logger.getLogger(clientName);
		logger.setLevel(Level.INFO);
		logger.info("Logging initialized");

		Container windowContent = this.getContentPane();
		windowContent.setLayout(new BorderLayout());
		windowContent.add(topBox, BorderLayout.NORTH);
		windowContent.add(pnlGameBoard, BorderLayout.CENTER);

		// On window-close, shut down the server
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				shutdownClient();
			}
		});

		// On button-click, start the client
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				startClient();
			}
		});

		this.pack();
		this.setVisible(true);
	}

	private void startClient() {
		try {
			// Connect to the server
			aigsServer = new Socket(txtIpAddress.getText(), new Integer(
					txtPortNumber.getText()));

			// Join the game
			playerName = getPlayer();
			MessageJoinGame msg = new MessageJoinGame(GameTypes.GAME_TicTacToe,
					txtGameName.getText(), 2, Character.toString(playerName));
			msg.send(aigsServer);
			MessageResult reply = Message.receive(
					AigsMessageTypes.MESSAGE_RESULT, aigsServer);

			if (reply.isOk()) {
				logger.info("Joined game successfully: " + reply.toString());

				// start play
				gamePosition = new GamePosition(3, 3);

				getServerMessage();

			} else {
				logger.info("Error joining game: " + reply.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void shutdownClient() {
		try {
			aigsServer.close();
		} catch (Exception e) {
		}
		this.dispose();
	}

	private void getServerMessage() {
		// Disable upper controls
		txtIpAddress.setEnabled(false);
		txtPortNumber.setEnabled(false);
		txtGameName.setEnabled(false);
		btnPlayerX.setEnabled(false);
		btnPlayerO.setEnabled(false);
		btnStart.setEnabled(false);

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					// Get game status from server
					MessageGameStatus msgStatus = Message.receive(
							AigsMessageTypes.MESSAGE_GAMESTATUS, aigsServer);

					// Convert the raw game position into a useful game position
					gamePosition.setRawPosition(msgStatus.getRawGamePosition());

					// Enable the buttons and update the status
					for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 3; j++) {
							char posValue = gamePosition.get(i, j);
							String btnValue = Character.toString(posValue);
							gameBoard[i][j].setText(btnValue);
						}
					}

					if (msgStatus.getGameStatus() == GameStatus.PLAYING
							.ordinal()) {
						logger.info("Move requested: " + msgStatus.toString());

						// Make our move
						int[] move = makeMove(gamePosition);
						
						// Display the move locally
						gameBoard[move[0]][move[1]].setText(Character.toString(playerName));

						// transmit the move to the server
						MessageGameMove msgMove = new MessageGameMove(move,
								playerName);
						logger.info("Move sent: " + msgMove.toString());
						try {
							msgMove.send(aigsServer);
							getServerMessage();
						} catch (Exception e) {
							logger.severe("Exception: " + e.toString());
						}
					} else {
						logger.info("Game over: " + msgStatus.toString());
					}
				} catch (Exception e) {
					logger.severe("Exception: " + e.toString());
				}
			}
		});
	}

	private enum Players {
		X, O
	};

	private char getPlayer() {
		char player;
		if (btnPlayerX.isSelected()) {
			player = 'X';
		} else {
			player = 'O';
		}
		return player;
	}
}
