package ch.fhnw.richards.aigs.tictactoe;

import ch.fhnw.richards.aigs.GamePosition;

public class FreeLinesClient extends BaseClient {
	private static final long serialVersionUID = 1;
	
	private char[][] position;
	private char playerName;

	// Main method - Create a client instance
	public static void main(String[] args) {
		new FreeLinesClient();
	}	

	// Constructor must call the super-constructor
	private FreeLinesClient() {
		super();
	}
	
	/**
	 * Strategy: play in the open square that has the largest number of free lines.
	 * A "free line" is a horizontal, vertical or diagonal line that contains no
	 * opposing pieces.
	 * 
	 * @return the move that we want to make
	 */
	@Override
	protected int[] makeMove(GamePosition gamePosition) {
		// Convert game position into a 3x3 array of characters
		position = convertGamePosition(gamePosition);
		
		// Who we are
		playerName = this.getPlayerName();
		
		// Use our strategy		
		int[] bestMove = new int[2];
		int[] move = new int[2];
		int bestFreeLines = -1;

		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				move[0] = x;
				move[1] = y;
				if (gamePosition.get(move) == ' ') {
					int freeLines = 0;
					
					// Check horizontal line
					boolean isFree = true;
					for (int i = 0; i < 3; i++) {
						if (position[i][y] != ' ' && position[i][y] != playerName) {
							isFree = false;
						}
					}
					if (isFree) freeLines++;
					
					
					// Check vertical line
					isFree = true;
					for (int i = 0; i < 3; i++) {
						if (position[x][i] != ' ' && position[x][i] != playerName) {
							isFree = false;
						}
					}
					if (isFree) freeLines++;
					
					// Check top-left-to-bottom-right diagonal
					if (x == y) { // on diagonal
						isFree = true;
						for (int i = 0; i < 3; i++) {
							if (position[i][i] != ' ' && position[i][i] != playerName) {
								isFree = false;
							}
						}
						if (isFree) freeLines++;
					}
					
					// Check top-right-to-bottom-left diagonal
					if ((2-x) == y) { // on diagonal
						isFree = true;
						for (int i = 0; i < 3; i++) {
							if (position[2-i][i] != ' ' && position[2-i][i] != playerName) {
								isFree = false;
							}
						}
						if (isFree) freeLines++;
					}
					
					// Do we have a better solution?
					if (freeLines > bestFreeLines) {
						bestMove[0] = x;
						bestMove[1] = y;
						bestFreeLines = freeLines;
					}
				}
			}
		}

		return bestMove;
	}
	


	/**
	 * Convert the raw game-position into a nice 3x3 array
	 * 
	 * @return the 3x3 array representing the current position
	 */
	private char[][] convertGamePosition(GamePosition gamePosition) {
		char[] rawPosition = gamePosition.getRawPosition();
		char[][] position = new char[3][3];
		for (int i = 0; i < 9; i++) {
			int x = i % 3;
			int y = i / 3;
			position[x][y] = rawPosition[i];
		}
		return position;
	}
}
