package ch.fhnw.richards.aigs.tictactoe;

import ch.fhnw.richards.aigs.GamePosition;

public class RandomClient extends BaseClient {
	private static final long serialVersionUID = 1;

	// Main method - Create a client instance
	public static void main(String[] args) {
		new RandomClient();
	}	

	// Constructor must call the super-constructor
	private RandomClient() {
		super();
	}
	
	/**
	 * This method contains the game-logic that determines where to play.
	 * We calculate our next move and return it as indices into the board. For
	 * example, to move to the center position, return [1,1]
	 * 
	 * This particular version plays in a random, empty position.
	 * 
	 * @return the move that we want to make
	 */
	@Override
	protected int[] makeMove(GamePosition gamePosition) {
		int[] move = new int[2];
		boolean moveFound = false;
		while (!moveFound) {
			move[0] = (int) (3 * Math.random());
			move[1] = (int) (3 * Math.random());
			moveFound = (gamePosition.get(move) == ' ');
		}		
		return move;
	}

}
