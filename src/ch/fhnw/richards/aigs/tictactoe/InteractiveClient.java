package ch.fhnw.richards.aigs.tictactoe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import ch.fhnw.richards.aigs.GamePosition;
import ch.fhnw.richards.aigs.gameLogic.GameStatus;
import ch.fhnw.richards.aigs.gameLogic.GameTypes;
import ch.fhnw.richards.aigs.messages.AigsMessageTypes;
import ch.fhnw.richards.aigs.messages.Message;
import ch.fhnw.richards.aigs.messages.MessageGameMove;
import ch.fhnw.richards.aigs.messages.MessageGameStatus;
import ch.fhnw.richards.aigs.messages.MessageJoinGame;
import ch.fhnw.richards.aigs.messages.MessageResult;

public class InteractiveClient extends JFrame {
	// Divide by 10 to get the displayed version number
	private static final long serialVersionUID = 1;

	// Instance number, in case multiple clients are running in the same JVM
	// Note: normally unnecessary, since each program runs in its own JVM.
	private static int instanceCounter = 0;
	private int instanceNumber;

	// Various important instance variables
	private String clientName; // Name displayed in title bar
	private Logger logger;     // Logger prints debug-info to console
	Socket aigsServer;         // Socket connected to AIGS-server
	char playerName;         // Which player are we? 'X' or 'O'

	// Important GUI controls
	private JTextField txtIpAddress;
	private JTextField txtPortNumber;
	private JRadioButton btnPlayerX;
	private JRadioButton btnPlayerO;
	private JTextField txtGameName;
	private Container pnlGameBoard;
	private JButton btnStart;

	// The game board, as JButtons
	JButton[][] gameBoard;
	GamePosition gamePosition = new GamePosition(3, 3);

	// Main method - Create a client instance
	public static void main(String[] args) {
		new InteractiveClient();
	}

	private InteractiveClient() {
		// Create upper GUI elements
		Box topBox = Box.createHorizontalBox();
		Box leftControls = Box.createVerticalBox();
		Box leftTopControls = Box.createHorizontalBox();
		Box leftBottomControls = Box.createHorizontalBox();
		leftControls.add(leftTopControls);
		leftControls.add(leftBottomControls);

		JLabel lblIpAddress = new JLabel("IP address");
		txtIpAddress = new JTextField("127.0.0.1");
		JLabel lblPort = new JLabel("Port");
		txtPortNumber = new JTextField("50001");
		leftTopControls.add(lblIpAddress);
		leftTopControls.add(Box.createHorizontalStrut(5));
		leftTopControls.add(txtIpAddress);
		leftTopControls.add(Box.createHorizontalStrut(15));
		leftTopControls.add(lblPort);
		leftTopControls.add(Box.createHorizontalStrut(5));
		leftTopControls.add(txtPortNumber);

		JLabel lblGameName = new JLabel("Game name");
		txtGameName = new JTextField("TicTacToe 1");
		JLabel lblPlayerNumber = new JLabel("Player");
		leftBottomControls.add(lblGameName);
		leftBottomControls.add(Box.createHorizontalStrut(5));
		leftBottomControls.add(txtGameName);
		leftBottomControls.add(Box.createHorizontalStrut(15));
		leftBottomControls.add(lblPlayerNumber);
		btnPlayerX = new JRadioButton(Players.X.toString());
		btnPlayerX.setSelected(true);
		leftBottomControls.add(btnPlayerX);
		btnPlayerO = new JRadioButton(Players.O.toString());
		leftBottomControls.add(btnPlayerO);
		ButtonGroup grpPlayer = new ButtonGroup();
		grpPlayer.add(btnPlayerX);
		grpPlayer.add(btnPlayerO);

		btnStart = new JButton("Connect");

		topBox.add(leftControls);
		topBox.add(Box.createHorizontalStrut(15));
		topBox.add(btnStart);
		topBox.setBorder(new EmptyBorder(5, 5, 5, 5));

		// Create game board (3x3)
		pnlGameBoard = new JPanel();
		pnlGameBoard.setPreferredSize(new Dimension(300, 300));
		pnlGameBoard.setLayout(new GridLayout(3, 3));
		gameBoard = new JButton[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				JButton tmpButton = new JButton("");
				tmpButton.setName(i + "," + j);
				gameBoard[i][j] = tmpButton;
				tmpButton.setEnabled(false);
				pnlGameBoard.add(tmpButton);

				// On button-click, play
				tmpButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						buttonPress((JButton) arg0.getSource());
					}
				});

			}
		}

		// Set up window
		instanceNumber = ++instanceCounter;
		clientName = "AIGS TicTacToe Client - v" + serialVersionUID / 10 + "." + serialVersionUID % 10
				+ " - " + instanceNumber;
		this.setTitle(clientName);

		// Set up logger
		logger = Logger.getLogger(clientName);
		logger.setLevel(Level.INFO);
		logger.info("Logging initialized");

		Container windowContent = this.getContentPane();
		windowContent.setLayout(new BorderLayout());
		windowContent.add(topBox, BorderLayout.NORTH);
		windowContent.add(pnlGameBoard, BorderLayout.CENTER);

		// On window-close, shut down the server
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				shutdownClient();
			}
		});

		// On button-click, start the client
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				startClient();
			}
		});

		this.pack();
		this.setVisible(true);
	}

	private void startClient() {
		try {
			// Connect to the server
			aigsServer = new Socket(txtIpAddress.getText(), new Integer(txtPortNumber.getText()));

			// Join the game
			playerName = getPlayer();
			MessageJoinGame msg = new MessageJoinGame(GameTypes.GAME_TicTacToe, txtGameName.getText(), 2,
					Character.toString(playerName));
			msg.send(aigsServer);
			MessageResult reply = Message.receive(AigsMessageTypes.MESSAGE_RESULT, aigsServer);

			if (reply.isOk()) {
				logger.info("Joined game successfully: " + reply.toString());

				// start play
				gamePosition = new GamePosition(3, 3);

				getServerMessage();

			} else {
				logger.info("Error joining game: " + reply.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void shutdownClient() {
		try {
			aigsServer.close();
		} catch (Exception e) {
		}
		this.dispose();
	}

	private void getServerMessage() {
		// Disable upper controls
		txtIpAddress.setEnabled(false);
		txtPortNumber.setEnabled(false);
		txtGameName.setEnabled(false);
		btnPlayerX.setEnabled(false);
		btnPlayerO.setEnabled(false);
		btnStart.setEnabled(false);

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					// Get game status from server
					MessageGameStatus msg = Message.receive(AigsMessageTypes.MESSAGE_GAMESTATUS, aigsServer);

					// Convert the raw game position into a useful game position
					gamePosition.setRawPosition(msg.getRawGamePosition());

					// Enable the buttons and update the status
					for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 3; j++) {
							char posValue = gamePosition.get(i, j);
							String btnValue = Character.toString(posValue);
							gameBoard[i][j].setText(btnValue);
						}
					}

					if (msg.getGameStatus() == GameStatus.PLAYING.ordinal()) {
						logger.info("Move requested: " + msg.toString());

						// Enable the buttons and update the status
						for (int i = 0; i < 3; i++) {
							for (int j = 0; j < 3; j++) {
								gameBoard[i][j].setEnabled(true);
							}
						}
					} else {
						logger.info("Game over: " + msg.toString());
					}
				} catch (Exception e) {
					logger.severe("Exception: " + e.toString());
				}
			}
		});
	}

	private void buttonPress(JButton src) {
		int[] indices = new int[2];

		// We encode the button's position in its name - this is an ugly hack...
		char[] srcName = src.getName().toCharArray();
		indices[0] = srcName[0] - '0';
		indices[1] = srcName[2] - '0';

		// display the move locally
		src.setText(Character.toString(playerName));

		// disable the buttons
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				gameBoard[i][j].setEnabled(false);
			}
		}

		// transmit the move to the server
		MessageGameMove msg = new MessageGameMove(indices, playerName);
		logger.info("Move sent: " + msg.toString());
		try {
			msg.send(aigsServer);
			getServerMessage();
		} catch (Exception e) {
			logger.severe("Exception: " + e.toString());
		}
	}
	
	private enum Players { X, O	};
	
	private char getPlayer() {
		char player;
		if (btnPlayerX.isSelected()) {
			player = 'X';
		} else {
			player = 'O';
		}
		return player;
	}
}
