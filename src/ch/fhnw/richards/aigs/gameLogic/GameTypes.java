package ch.fhnw.richards.aigs.gameLogic;

public enum GameTypes {
	GAME_TicTacToe(GameTicTacToe.class),
	GAME_Go(GameGo.class);
	
	private final Class<? extends Game> gameType;
	
	// constructor
	GameTypes(Class<? extends Game> gameType) {
		this.gameType = gameType;
	}
	
	public Class<? extends Game> getGameType() {
		return gameType;
	}
}
