package ch.fhnw.richards.aigs.gameLogic;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Logger;

import ch.fhnw.richards.aigs.AigsException;
import ch.fhnw.richards.aigs.GamePosition;
import ch.fhnw.richards.aigs.Player;
import ch.fhnw.richards.aigs.Server;
import ch.fhnw.richards.aigs.messages.MessageGameMove;
import ch.fhnw.richards.aigs.messages.MessageGameStatus;

public abstract class Game extends Thread {
	// List of all games currently active
	private static HashMap<String, Game> games = new HashMap<String, Game>();

	// Instance attributes
	private String gameID;
	protected int maxPlayers;
	protected GameStatus status;
	protected String winner;
	private ArrayList<Player> players;

	GamePosition gamePosition;

	/**
	 * Subclasses must implement a method to accept and process a move
	 */
	public abstract boolean move(Player p, MessageGameMove m);

	public static void addGame(String id, Game newGame) {
		synchronized (games) {
			if (!games.containsKey(id)) {
				games.put(id, newGame);
			}
		}
	}

	public static void removeGame(Game oldGame) {
		synchronized (games) {
			games.remove(oldGame.getGameId());
		}
	}

	public static int getNumGames() {
		return games.size();
	}
	
	public static HashMap<String, Game> getGameList() {
		return games;
	}

	public static <G extends Game> G joinOrCreateGame(GameTypes type, String gameID,
			int maxPlayers, Player newPlayer) throws AigsException {
		G g = null;

		try {
			// Only proceed if the game does not exist, or exists with the correct
			// type
			Game tmpGame = games.get(gameID);
			if (tmpGame == null) {
				Class<? extends Game> gameClass = type.getGameType();
				Constructor<? extends Game> c = gameClass.getConstructor(String.class);
				g = (G) c.newInstance(gameID);
				g.setMaxPlayers(maxPlayers);
				Game.addGame(gameID, g);
			} else if (tmpGame.getClass() == type.getGameType()) {
				g = (G) tmpGame;

				if (g.status.compareTo(GameStatus.PLAYING) >= 0) {
					throw new AigsException("Game already started");
				} else if (g.getNumPlayers() >= maxPlayers) {
					throw new AigsException("Game is already full");
				}				
			} else {
				throw new AigsException("Game exists, but is of type " + tmpGame.getClass());
			}
		} catch (SecurityException e) {
			Logger.getLogger(Server.APP_TITLE).severe(e.toString());
		} catch (NoSuchMethodException e) {
			Logger.getLogger(Server.APP_TITLE).severe(e.toString());
		} catch (IllegalArgumentException e) {
			Logger.getLogger(Server.APP_TITLE).severe(e.toString());
		} catch (InstantiationException e) {
			Logger.getLogger(Server.APP_TITLE).severe(e.toString());
		} catch (IllegalAccessException e) {
			Logger.getLogger(Server.APP_TITLE).severe(e.toString());
		} catch (InvocationTargetException e) {
			Logger.getLogger(Server.APP_TITLE).severe(e.toString());
		}

		g.addPlayer(newPlayer); // Add player to game
		return g;
	}

	protected Game(GameTypes type, String id) {
		this.gameID = id;
		players = new ArrayList<Player>();
		status = GameStatus.SETUP;
		winner = "none";
	}

	/**
	 * Add a new player to our local set of players. We really would like a "set",
	 * to prevent adding the same player twice. However, we also need to be able
	 * to iterate through the players using an index. Hence, we do the
	 * non-duplicate check ourselves.
	 * 
	 * @param newPlayer
	 */
	public void addPlayer(Player newPlayer) throws AigsException {
		synchronized (players) {
			boolean found = false;
			for (Player oldPlayer : players) {
				if (oldPlayer.equals(newPlayer)) {
					throw new AigsException("Duplicate player");
				}
			}

			if (!found) players.add(newPlayer);

			// If the game is now full, start playing the game
			if (players.size() == this.maxPlayers) this.start();
		}
	}

	public int getNumPlayers() {
		synchronized (players) {
			return players.size();
		}
	}
	
	public ArrayList<Player> getPlayerList() {
		return players;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public MessageGameStatus makeGameStatusMessage() {
		int statusOrdinal = status.ordinal();
		return new MessageGameStatus(gamePosition.getRawPosition(), statusOrdinal,
				winner);
	}

	public String getGameId() {
		return gameID;
	}

	@Override
	public boolean equals(Object o) {
		Game g = (Game) o;
		return gameID.equals(g.gameID);
	}

	/**
	 * Iterate through the players (one player per iteration), and continue until
	 * the game ends for some reason.
	 * 
	 * When the game is over, send the final result to each player, then terminate
	 * the connections, clean up, and remove the game from the active list.
	 */
	@Override
	public void run() {
		this.status = GameStatus.PLAYING;
		Logger.getLogger(Server.APP_TITLE).info("Game started");

		int currentPlayer = -1;
		while (this.status == GameStatus.PLAYING) {
			// Move to the next player
			currentPlayer++;
			if (currentPlayer >= players.size()) currentPlayer = 0;
			Player p = players.get(currentPlayer);

			// Send the player the current position, get the next move,
			// and process this in the subclass. If the player sends
			// an invalid move, we re-send the game position. This loops
			// until the player realizes the problem...
			boolean validMove = false;
			while (!validMove) {
				p.sendPosition();
				MessageGameMove move = p.getMove();
				validMove = this.move(p, move);
			}
		}
		for (Iterator<Player> i = players.iterator(); i.hasNext();) {
			Player p = i.next();

			// Tell each players that the game is over
			p.gameOver();

			// Remove players from our local players list
			i.remove();

			// Remove players from Player.players
			Player.removePlayer(p);
		}

		// Remove this game from Game.games
		Game.removeGame(this);
	}
}
