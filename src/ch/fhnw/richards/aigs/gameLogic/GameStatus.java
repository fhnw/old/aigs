package ch.fhnw.richards.aigs.gameLogic;

public enum GameStatus {
	SETUP, PLAYING, INTERRUPTED, DRAWN, WON;
}
