package ch.fhnw.richards.aigs.gameLogic;

import java.util.logging.Logger;

import ch.fhnw.richards.aigs.GamePosition;
import ch.fhnw.richards.aigs.Player;
import ch.fhnw.richards.aigs.Server;
import ch.fhnw.richards.aigs.messages.MessageGameMove;

public class GameTicTacToe extends Game {
	public GameTicTacToe(String id) {
		super(GameTypes.GAME_TicTacToe, id);
		this.gamePosition = new GamePosition(3, 3);
	}

	@Override
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = 2;
	}

	@Override
	public boolean move(Player p, MessageGameMove m) {
		boolean allOk = true;
		String pName = p.getPlayerName();
		String pieceName = Character.toString(m.getMovePiece());
		if (!pName.equals(pieceName)) {
			Logger.getLogger(Server.APP_TITLE).info("Player name does not match piece name");
			allOk = false;
		} else {
			int[] move = m.getMovePosition();
			if (move.length != 2) {
				Logger.getLogger(Server.APP_TITLE).info("Invalid move");
				allOk = false;
			} else if (move[0] < 0 || move[0] > 2 || move[1] < 0 || move[1] > 2) {
				Logger.getLogger(Server.APP_TITLE).info("Invalid move");
				allOk = false;
			} else if (this.gamePosition.get(move) != ' ') {
				Logger.getLogger(Server.APP_TITLE).info("Invalid move: square not empty");
				allOk = false;
			} else { // All ok, accept move
				this.gamePosition.set(m.getMovePiece(), move);
				allOk = true;
			}
		}

		// Check if the game is won

		for (int col = 0; col < 3; col++) { // columns
			if (gamePosition.get(col, 0) != ' ') {
				if ((gamePosition.get(col, 0) == gamePosition.get(col, 1))
						&& (gamePosition.get(col, 0) == gamePosition.get(col, 2))) {
					this.status = GameStatus.WON;
					this.winner = Character.toString(gamePosition.get(col, 0));
				}
			}
		}
		if (this.status == GameStatus.PLAYING) {
			for (int row = 0; row < 3; row++) { // rows
				if (gamePosition.get(0, row) != ' ') {
					if ((gamePosition.get(0, row) == gamePosition.get(1, row))
							&& (gamePosition.get(0, row) == gamePosition.get(2, row))) {
						this.status = GameStatus.WON;
						this.winner = Character.toString(gamePosition.get(0, row));
					}
				}
			}
		}
		if (this.status == GameStatus.PLAYING) {
			if (gamePosition.get(1, 1) != ' ') { // diagonals
				if (((gamePosition.get(0, 0) == gamePosition.get(1, 1)) && (gamePosition.get(0, 0) == gamePosition
						.get(2, 2)))
						|| ((gamePosition.get(2, 0) == gamePosition.get(1, 1)) && (gamePosition.get(1, 1) == gamePosition
								.get(0, 2)))) {
					this.status = GameStatus.WON;
					this.winner = Character.toString(gamePosition.get(1, 1));
				}
			}
		}

		// If not won, check if the game is drawn (cat's game)
		if (this.status == GameStatus.PLAYING) {
			int numUnplayedFields = 0;
			char[] rawPosition = this.gamePosition.getRawPosition();
			for (char field : rawPosition) {
				if (field == ' ') numUnplayedFields++;
			}
			if (numUnplayedFields == 0) this.status = GameStatus.DRAWN;
		}

		return allOk;
	}
}