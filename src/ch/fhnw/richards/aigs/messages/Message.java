package ch.fhnw.richards.aigs.messages;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public abstract class Message {
	AigsMessageTypes type;
	
	public Message(AigsMessageTypes type) {
		this.type = type;
	}
	
	/**
	 * Receive a message: create a message object and fill it using data transmitted over the given
	 * socket.
	 * 
	 * @param socket
	 *          the socket to read from
	 * @return the new message object, or null in case of an error
	 * @throws Exception
	 */
	public static <T extends Message> T receive(AigsMessageTypes msgType, Socket socket) throws Exception {
		Serializer netIn = new Persister();
		InputStream in = socket.getInputStream();
		return (T) netIn.read(msgType.getMessageClass(), in);
	}
	
	/**
	 * Send the current message.
	 * 
	 * @param socket
	 *          the socket to write to
	 * @throws Exception
	 */
	public void send(Socket socket) throws Exception {
		Serializer netOut = new Persister();
		OutputStream out = socket.getOutputStream();
		netOut.write(this, out);
		out.flush();
	}
	
	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append(this.type.getXmlString());
		
		return str.toString();
	}
	
}
