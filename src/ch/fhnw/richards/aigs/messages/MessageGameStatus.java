package ch.fhnw.richards.aigs.messages;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import ch.fhnw.richards.aigs.GamePosition;

@Root
public class MessageGameStatus extends Message {
	@ElementArray
	private char[] rawGamePosition;

	@Element
	private int gameStatus;

	@Element
	private String winningPlayer;

	/**
	 * Constructor for use by the SimpleXML framework
	 * 
	 * @param rawGamePosition
	 * @param gameStatus
	 * @param winningPlayer
	 */
	public MessageGameStatus(@ElementArray(name = "rawGamePosition") char[] rawGamePosition,
			@Element(name = "gameStatus") int gameStatus,
			@Element(name = "winningPlayer") String winningPlayer) {
		super(AigsMessageTypes.MESSAGE_GAMESTATUS);
		this.rawGamePosition = rawGamePosition;
		this.gameStatus = gameStatus;
		this.winningPlayer = winningPlayer;
	}

	/**
	 * Constructor for use by the server
	 * 
	 * @param gamePosition
	 * @param gameStatus
	 * @param winningPlayer
	 */
	public MessageGameStatus(GamePosition gamePosition, int gameStatus, String winningPlayer) {
		super(AigsMessageTypes.MESSAGE_GAMESTATUS);
		this.rawGamePosition = gamePosition.getRawPosition();
		this.gameStatus = gameStatus;
		this.winningPlayer = winningPlayer;
	}

	public char[] getRawGamePosition() {
		return rawGamePosition;
	}

	public void setRawGamePosition(char[] rawGamePosition) {
		this.rawGamePosition = rawGamePosition;
	}

	public int getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(int gameStatus) {
		this.gameStatus = gameStatus;
	}

	public String getWinningPlayer() {
		return winningPlayer;
	}

	public void setWinningPlayer(String winningPlayer) {
		this.winningPlayer = winningPlayer;
	}
	
	@Override
	public String toString() {
		StringBuffer gameStatusInfo = new StringBuffer();
		gameStatusInfo.append(super.toString());
		gameStatusInfo.append(": gameStatus=" + gameStatus + ", winningPlayer=" + winningPlayer);
		gameStatusInfo.append("\n[");
		for (char i : rawGamePosition) {
			gameStatusInfo.append(i + ",");
		}
		gameStatusInfo.append("]");		
		return gameStatusInfo.toString();
	}
}
