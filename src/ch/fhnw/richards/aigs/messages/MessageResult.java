package ch.fhnw.richards.aigs.messages;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

@Root
public class MessageResult extends Message {
	@Element
	private boolean ok;

	@Element
	private String description;

	public MessageResult(@Element(name = "ok") boolean ok,
			@Element(name = "description") String description) {
		super(AigsMessageTypes.MESSAGE_RESULT);
		this.ok = ok;
		this.description = description;
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return super.toString() + ": ok=" + ok + ", " + description;
	}
}
