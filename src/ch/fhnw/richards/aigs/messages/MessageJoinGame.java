package ch.fhnw.richards.aigs.messages;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import ch.fhnw.richards.aigs.gameLogic.Game;
import ch.fhnw.richards.aigs.gameLogic.GameTypes;

@Root
public class MessageJoinGame extends Message {
	@Element
	private GameTypes gameType;

	@Element
	private String gameID;

	@Element
	private int numPlayers;

	@Element
	private String playerName;

	public MessageJoinGame(@Element(name = "gameType") GameTypes gameType,
			@Element(name = "gameID") String gameID, @Element(name = "numPlayers") int numPlayers,
			@Element(name = "playerName") String playerName) {
		super(AigsMessageTypes.MESSAGE_JOINGAME);
		this.gameType = gameType;
		this.gameID = gameID;
		this.numPlayers = numPlayers;
		this.playerName = playerName;
	}

	public GameTypes getGameType() {
		return gameType;
	}

	public void setGameType(GameTypes gameType) {
		this.gameType = gameType;
	}

	public String getGameID() {
		return gameID;
	}

	public void setGameID(String gameID) {
		this.gameID = gameID;
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	public void setNumPlayers(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	@Override
	public String toString() {
		return super.toString() + ": gameType=" + gameType + ", gameID=" + gameID + ", numPlayers=" + numPlayers + ", playerID=" + playerName;
	}
}
