package ch.fhnw.richards.aigs.messages;

public enum AigsMessageTypes {
	MESSAGE_JOINGAME("JoinGame", 4, MessageJoinGame.class),
	MESSAGE_GAMEMOVE("GameMove", 2, MessageGameMove.class),
	MESSAGE_GAMESTATUS("GameStatus", 3, MessageGameStatus.class),
	MESSAGE_RESULT("Result", 2, MessageResult.class);

	private final String xmlString;
	private final int numParms;
	private final Class<? extends Message> msgClass;

	// constructor
	AigsMessageTypes(String xmlString, int numParms, Class<? extends Message> msgClass) {
		this.xmlString = xmlString;
		this.numParms = numParms;
		this.msgClass = msgClass;
	}

	public String getXmlString() {
		return xmlString;
	}

	public int getnumParms() {
		return numParms;
	}
	
	public Class<? extends Message> getMessageClass() {
		return msgClass;
	}
}
