package ch.fhnw.richards.aigs.messages;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

@Root
public class MessageGameMove extends Message {
	@ElementArray
	private int[] movePosition;
	
	@Element
	private char movePiece;
	
	public MessageGameMove(@ElementArray(name = "movePosition") int[] movePosition,
			@Element(name = "movePiece") char movePiece) {
		super(AigsMessageTypes.MESSAGE_GAMESTATUS);
		this.movePosition = movePosition;
		this.movePiece = movePiece;
	}

	public int[] getMovePosition() {
		return movePosition;
	}

	public void setMovePosition(int[] movePosition) {
		this.movePosition = movePosition;
	}

	public char getMovePiece() {
		return movePiece;
	}

	public void setMovePiece(char movePiece) {
		this.movePiece = movePiece;
	}
	
	@Override
	public String toString() {
		StringBuffer gameMoveInfo = new StringBuffer();
		gameMoveInfo.append(super.toString());
		gameMoveInfo.append(": movePiece=" + movePiece + ", movePosition=[");
		for (int i : movePosition) {
			gameMoveInfo.append(i + ",");
		}
		gameMoveInfo.append("]");		
		return gameMoveInfo.toString();
	}
}
