package ch.fhnw.richards.aigs;

import java.net.Socket;
import java.util.logging.Logger;

import ch.fhnw.richards.aigs.messages.*;

/**
 * This class provides a common messaging framework that can be used as the basis for player-objects
 * on both the server and client sides.
 * 
 * The AIGS messaging framework allows both server- and client-initiated actions. In other words,
 * either side may send a message that requires a response; this may happen simultaneously, with the
 * client requesting information from the server at the same time that the server is requesting
 * something from the client.
 * 
 * The possible messages are defined in the enumerated type AigsMessages.
 * 
 * @author brad
 * 
 */
public abstract class AbstractPlayer {
	private Socket playerSocket;

	protected AbstractPlayer(Socket playerSocket) {
		this.playerSocket = playerSocket;
	}

	protected Message receive(AigsMessageTypes messageType) {
		Message msg;
		try {
			msg = Message.receive(messageType, playerSocket);
		} catch (Exception e) {
			msg = null;
		}
		Logger.getLogger(Server.APP_TITLE).info("Received: " + msg.toString());
		return msg;
	}

	protected void send(Message msg) {
		try {
			msg.send(playerSocket);
			Logger.getLogger(Server.APP_TITLE).info("Sent: " + msg.toString());
		} catch (Exception e) {
			Logger.getLogger(Server.APP_TITLE).severe("Error sending message: " + e.toString());
		}
	}

	protected void close() {
		try {
			if (playerSocket != null) {
				playerSocket.close();
			}
		} catch (Exception e) {
		}
	}
}
