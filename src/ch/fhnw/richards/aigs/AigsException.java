package ch.fhnw.richards.aigs;

public class AigsException extends Exception {
	public AigsException(String msg) {
		super(msg);
	}
}
