package ch.fhnw.richards.aigs;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import ch.fhnw.richards.aigs.gameLogic.Game;

/**
 * Provides the GUI of the AIGS server
 * 
 * @author brad
 * 
 */
public class Server extends JFrame {
	// Divide by 10 to get the displayed version number
	private static final long serialVersionUID = 1;

	// App title
	public static final String APP_TITLE = "AIGS - v" + serialVersionUID / 10 + "."
			+ serialVersionUID % 10;

	// Singleton class instance
	private static Server server;

	// Important GUI controls
	private JTextField txtPortNumber;
	private JButton btnStart;
	JTextField txtNumGames;
	JTextField txtNumPlayers;
	JTree treeGamesPlayers;
	DefaultMutableTreeNode treeRoot;

	// Server components
	private Monitor monitor;
	private GameManager gameManager;

	// Only create a new server instance if none exists
	public static void main(String[] args) {
		if (server == null) {
			server = new Server();
		}
	}

	private Server() {
		// Create upper GUI elements
		JLabel lblPort = new JLabel("Port for incoming connections");
		txtPortNumber = new JTextField("50001");
		btnStart = new JButton("Start AIGS");
		Box topBox = Box.createHorizontalBox();
		topBox.setBorder(new EmptyBorder(5, 5, 5, 5));
		topBox.add(lblPort);
		topBox.add(Box.createHorizontalStrut(5));
		topBox.add(txtPortNumber);
		topBox.add(Box.createHorizontalStrut(15));
		topBox.add(btnStart);
		
		// Create lower GUI elements
		JLabel lblNumGames = new JLabel("Number of games");
		JLabel lblNumPlayers = new JLabel("Number of players");
		txtNumGames = new JTextField("0");
		txtNumGames.setEnabled(false);
		txtNumPlayers = new JTextField("0");
		txtNumPlayers.setEnabled(false);
		Box bottomBox = Box.createHorizontalBox();
		bottomBox.setBorder(new EmptyBorder(5, 5, 5, 5));
		bottomBox.add(lblNumGames);
		bottomBox.add(Box.createHorizontalStrut(5));
		bottomBox.add(txtNumGames);
		bottomBox.add(Box.createHorizontalStrut(5));
		bottomBox.add(lblNumPlayers);
		bottomBox.add(Box.createHorizontalStrut(5));
		bottomBox.add(txtNumPlayers);
		
		// Create tree
		treeRoot = new DefaultMutableTreeNode("root");
		DefaultTreeModel treeModel = new DefaultTreeModel(treeRoot);
		treeGamesPlayers = new JTree(treeModel);
		treeGamesPlayers.setRootVisible(false);
		JScrollPane paneGamesPlayers = new JScrollPane(treeGamesPlayers);
		// TODO JTree behavior is pretty awful...

		// Set up window
		this.setTitle(APP_TITLE);

		Container windowContent = this.getContentPane();
		windowContent.setLayout(new BorderLayout());
		windowContent.add(topBox, BorderLayout.NORTH);
		windowContent.add(paneGamesPlayers, BorderLayout.CENTER);
		windowContent.add(bottomBox, BorderLayout.SOUTH);

		// On window-close, shut down the server
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				shutdownServer();
			}
		});

		// On button-click, start the server
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				startServer();
			}
		});

		this.pack();
		this.setVisible(true);

		// Set up logging
		Logger logger = Logger.getLogger(APP_TITLE);
		logger.setLevel(Level.INFO);
		logger.info("Logging initialized");
	}

	private void startServer() {
		int portNumber = Integer.parseInt(txtPortNumber.getText());
		if (portNumber > 1024 & portNumber <= 65535) {
			// Disable upper controls
			txtPortNumber.setEnabled(false);
			btnStart.setEnabled(false);

			// The games-manager handles network connections, and manages games and players
			gameManager = new GameManager(portNumber);
			gameManager.start();

			// The monitor updates the JTable with information about current games
			monitor = new Monitor(this);
			monitor.start();
		}
	}

	private void shutdownServer() {
		// Stop the monitor
		if (monitor != null) monitor.stopMonitor();

		// Stop the game-manager
		if (gameManager != null) gameManager.stopGameManager();

		// Close the GUI
		this.dispose();
	}
}
