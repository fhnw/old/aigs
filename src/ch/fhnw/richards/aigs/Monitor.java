package ch.fhnw.richards.aigs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import ch.fhnw.richards.aigs.gameLogic.Game;

/**
 * This class monitors the list of current games, and regularly updates a JTable in
 * the main GUI.
 * 
 * @author brad
 *
 */
public class Monitor extends Thread {
	private Server aigsServer;
	
	// Variable to control thread execution
	private boolean runMonitor;
	
	public Monitor(Server aigsServer) {
		this.aigsServer = aigsServer;
		
		this.setName("AIGS monitor");
		runMonitor = true;
	}
	
	public void stopMonitor() {
		runMonitor = false;
		this.interrupt();
	}
	
	@Override
	public void run() {
		while(runMonitor) {
			aigsServer.txtNumGames.setText(Integer.toString(Game.getNumGames()));
			aigsServer.txtNumPlayers.setText(Integer.toString(Player.getNumPlayers()));

			// TODO Update JTree
			
			DefaultMutableTreeNode root = aigsServer.treeRoot;
			root.removeAllChildren();
			HashMap<String, Game> gameList = Game.getGameList();
			synchronized(gameList) {
				Collection<Game> games = gameList.values();
				for (Iterator<Game> i = games.iterator(); i.hasNext(); ) {
					Game g = i.next();
					DefaultMutableTreeNode gameNode = new DefaultMutableTreeNode(g.getName());
					root.add(gameNode);
					
					for (Player p : g.getPlayerList()) {
						DefaultMutableTreeNode playerNode = new DefaultMutableTreeNode(p.getPlayerName());
						gameNode.add(playerNode);
					}
				}
			}
			aigsServer.treeGamesPlayers.updateUI();
			
			try {
				sleep(3000);
			} catch (InterruptedException e) {
			}
		}
	}
}
